package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Random;

@SpringBootApplication
@RestController
public class DiscussionApplication {
	private ArrayList<User> user = new ArrayList<>();
	int i = 0;
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}
	private static Random random = new Random();
	private static String[] firstNames = {"John", "Emma", "Olivia", "Ava", "Isabella", "Sophia", "Robin"};
	String randomName = firstNames[random.nextInt(firstNames.length)];

	/*	@RequestMapping(value="/posts", method = RequestMethod.GET)
	public ArrayList<String> getall(){
		return str;

	}*/
	// GET ALL POST
	@RequestMapping(value="/posts", method = RequestMethod.GET)
	public String  getallPost(){
		return "all post retrived";
	}
	//GET A POST
	@RequestMapping(value="/posts/{postid}", method = RequestMethod.GET)
	public String getPost(@PathVariable Long postid){
		return "post id = "+postid;
	}
	//DELETE POSTID
	@RequestMapping(value="/posts/{postid}", method = RequestMethod.DELETE)
	public String deletePost(@PathVariable Long postid){
		return "deleted id = "+postid;
	}
	// CREATE POST
	@RequestMapping(value="/posts", method = RequestMethod.POST)
	public String createPost(){
		return "new post created";
	}
	//UPDATE POST
	@RequestMapping(value="/posts/{postid}", method = RequestMethod.PUT)
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}
	// GET MY POST
	@RequestMapping(value="/myPosts", method = RequestMethod.GET)
	public String  getmyPost(@RequestHeader(value = "Authorization")String user){
		return "post for "+user+" has been read";
	}

	//ASSIGNMENT

	//Create a user
	@PostMapping("/users")
	public String createUser(){
		i++;
		User newuser = new User(i+"",firstNames[random.nextInt(firstNames.length)]);
		user.add(newuser);
		return "new user added";
	}
	//Get all the user
	@GetMapping("/users")
	public String getallUser(){
		return "all users retrieved";
	}
	// Get a specific user
	@GetMapping("/users/{userID}")
	public String getUser(@PathVariable String userID){
		String message = "ID: "+userID+" was not found";
		for(User users: user){
			if(users.getId().equals(userID)){
				message = (users.getName()+"'s data was retrieved with the ID :"+ users.getId());
				break;
			}
		}
		return message;
	}
	//Delete a user

	@DeleteMapping("/users/{userID}")
	public String  deleteUser(@RequestHeader(value = "Authorization")String userID){
		String message ="not auhtorized or not found";
		for(User users: user){
			if(users.getId().equals(userID)){
				message = "Data was deleted with the ID :"+ userID;
				user.remove(users);
				break;
			}
		}
		return message;
	}
	//update user
	@PutMapping("/users/{userID}")
	public User updateUser(@PathVariable String userID, @RequestBody User oUser){
		User newUser = new User();
		for(User users: user){
			if(users.getId().equals(userID)){
				users.setId(userID);
				users.setName(oUser.getName());
				newUser = users;
				break;
			}
		}
		return newUser;
	}


}


